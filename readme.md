# Tutorial

Building RESTful Python Web Services

ISBN 978-1-78646-225-1

## Environment setup
Creating a virtual environment...
```
python3 -m venv ~/Documents/building_restful_python_web_services/DjangoTutorial
```
...and activating it
```
source ~/Documents/building_restful_python_web_services/DjangoTutorial/bin/activate
```
## Django cheat-sheet:
Create new project
```
django-admin.py startproject <myproject>
```
Create new app
```
python manage.py startapp <myapp>
```
Create migrations
```
python manage.py makemigrations <myapp>
```
Apply migrations
```
python manage.py migrate
```
Launch python shell
```
python manage.py shell
```
Start app server
```
python manage.py runserver
```
```
python manage.py runserver x.x.x.x:port
```
Testing (with coverage)
```
python manage.py test -v 2
```
```
coverage report -m
```
```
coverage html
```

